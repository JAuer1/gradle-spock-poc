# Gradle-Spock-POC

A super simple, barebones application that runs Spock specifications as opposed to JUnit 4 test cases.

To run, just execute `./gradlew clean test`.

Please visit build.gradle for the dependencies and plugins needed.

Please contact joshua.auer1@t-moblie.com with any questions.