package main;

public class ExampleMain {

    public int operate(int a, int b, String operand) {
        int result;
        switch (operand.charAt(0)) {
            case '+':
                result = sum(a,b);
                break;
            case '-':
                result = subtract(a,b);
                break;
            default:
                result = 0;
                break;
        }
        return result;
    }

    private int sum(int a, int b) {
        return a + b;
    }

    private int subtract(int a, int b) {
        return a - b;
    }
}
