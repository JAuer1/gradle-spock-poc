package main

import spock.lang.Specification
import spock.lang.Unroll

class ExampleSpec extends Specification {

    ExampleMain example = new ExampleMain()

    @Unroll
    def "This is an unroll Specification for sum"() {
        expect: "two numbers sum correctly"
        result == example.sum(a, b)

        where:
        a | b || result
        1 | 1 || 2
        1 | 2 || 3
        2 | 3 || 5
    }

    @Unroll
    def "This is an unroll Specification for subtract"() {
        expect: "two numbers subtract correctly"
        result == example.subtract(a, b)

        where:
        a | b || result
        1 | 1 || 0
        1 | 2 || -1
        6 | 3 || 3
    }

    @Unroll
    def "This test case ensures that the operate method works as intended"() {
        expect: "Operate works correctly with implemented operands"
        result == example.operate(a, b, operand)

        where:
        a | b | operand || result
        1 | 1 | '+'     || 2
        4 | 1 | '-'     || 3
        2 | 3 | '*'     || 0
        9 | 3 | '/'     || 0
    }
}
